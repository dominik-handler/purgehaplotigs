#!/usr/bin/env Rscript

# Copyright (c) 2017 Michael Roach (Australian Wine Research Institute)
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

# Usage: gen_histogram.Rscript  hist.csv  out.png
# hist file should be col.1: read depth, col.2: count

library(ggplot2)

# input args
args = commandArgs(trailingOnly=TRUE)
csvFile = args[1]
pngOut = args[2]
maxDepth = as.numeric(args[3])

# check libraries for rendering image
avLibs = capabilities()

if(avLibs['cairo']){
    options(bitmapType='cairo')
} else if(avLibs['X11']){
    options(bitmapType='Xlib')
} else {
    stop('neither X11 nor cairo available for rendering histogram')
}

# calc step sizes
DAT=read.csv(file=csvFile,header=F,sep=",")
stepLen = ceiling(maxDepth / 40)
steps = seq(0, maxDepth, stepLen)

png(filename = args[2], width = 1500, height = 900, units = "px")

ggplot() +
    geom_path(aes(x = DAT$V1, y = DAT$V2), size = 1) +
    scale_x_continuous(name = "Read-Depth", breaks = steps, expand=c(0.01,0.01)) +
    scale_y_continuous(name = "Count", expand=c(0.01,0.01))+
    theme(
        panel.background = element_blank(), 
        axis.line = element_line(colour = "black"), 
        panel.grid.major.x = element_line(colour = "#969696"),
        panel.grid.minor.x = element_line(colour = "#bdbdbd"),
        axis.text.x = element_text(angle=45, hjust=1, size=12, colour = "black"),
        axis.text.y = element_text(colour = "black", size=12),
        axis.title = element_text(colour = 'black', size = 14))
