[![install with bioconda](https://img.shields.io/badge/install%20with-bioconda-brightgreen.svg?style=flat-square)](http://bioconda.github.io/recipes/purge_haplotigs/README.html)
# Purge Haplotigs

Pipeline to help with curating heterozygous diploid genome assemblies (for instance when assembling using FALCON or FALCON-unzip). 

## Contents

 - [Introduction](#markdown-header-introduction)
 - [Dependencies](#markdown-header-dependencies)
 - [Resource Usage](#markdown-header-resource-usage)
 - [Installation](#markdown-header-installation)
 - [Running Purge Haplotigs](#markdown-header-running-purge-haplotigs)
 - [Optional Extras](#markdown-header-optional-extras)
 - [Citation](#markdown-header-citation)
 - [Updates](#markdown-header-updates)


## Introduction

#### The problem

Some parts of a genome may have a very high degree of heterozygosity. 
This causes contigs for both haplotypes of that part of the genome to be assembled as separate primary contigs, rather than as a contig and an associated haplotig. 
This can be an issue for downstream analysis whether you're working on the haploid or phased-diploid assembly.

#### The solution

Identify pairs of contigs that are syntenic and move one of them to the haplotig 'pool'. 
The pipeline uses mapped read coverage and Minimap2 alignments to determine which contigs to keep for the haploid assembly. 
Dotplots are optionally produced for all flagged contig matches, juxtaposed with read-coverage, to help the user determine the proper assignment of any remaining ambiguous contigs. 
The pipeline will run on either a haploid assembly (i.e. Canu, FALCON or FALCON-Unzip primary contigs) or on a phased-diploid assembly (i.e. FALCON-Unzip primary contigs + haplotigs). 
Here are [two examples](https://bitbucket.org/mroachawri/purge_haplotigs/wiki/Examples) of how Purge Haplotigs can improve a haploid and diploid assembly.

## Dependencies

- Bash
- BEDTools (tested with v2.26.0)
- SAMTools (tested with v1.7)
- Minimap2 (tested with v2.11/v2.12, https://github.com/lh3/minimap2)
- Perl (with core modules: FindBin, Getopt::Long, Time::Piece, threads, Thread::Semaphore, Thread::Queue, List::Util)
- Rscript (with ggplot2)

## Resource Usage

Updated benchmarks are shown below using the current pipeline (v1.1.1) against the four assemblies originally used in [Roach et al. 2018](https://doi.org/10.1186/s12859-018-2485-7).
An 8-core/16-thread (AMD Ryzen 7 2700x) workstation with 32 GB of RAM was used and all 16 threads were supplied for steps 1 and 3. 

Assembly | Diploid size (Mbp) | Runtime (hh:mm:ss) | Peak RAM (GB)
---|---|---|---|---
[*Clavicorona pyxidata*](https://doi.org/10.1038/nmeth.4035) | 65.4 | 00:00:27 | 2 
[*Arabidopsis thaliana* (Cvi-0 x Col-0)](https://doi.org/10.1038/nmeth.4035) | 245 | 00:01:58 | 8
[*Vitis vinifera L. Cv.* Cabernet Sauvignon](https://doi.org/10.1038/nmeth.4035) | 959 | 00:13:49 | 29 
[*Taeniopygia guttata*](https://doi.org/10.1093/gigascience/gix085) | 1 983 | 00:16:56 | 29

## Installation

Currently only tested on Ubuntu, there is a [Detailed manual installation](https://bitbucket.org/mroachawri/purge_haplotigs/wiki/Install) example for Ubuntu 16.04 LTS in the wiki.

### Easy Installation using bioconda

- Add the bioconda and conda-forge channels if you haven't already
```
#!text
conda config --add channels defaults
conda config --add channels bioconda
conda config --add channels conda-forge
```

- create (if needed) and activate a conda environment

```
#!text
conda create -n purge_haplotigs_env
conda activate purge_haplotigs_env
```

- Install

`conda install purge_haplotigs`

- Test Purge Haplotigs

`purge_haplotigs test`



### Manual Installation

- Install dependencies, make sure they're in the system PATH
- Pull/clone this git
- Either softlink `purge_haplotigs` to a directory in your system PATH

`ln -s /path/to/purge_haplotigs/bin/purge_haplotigs ~/bin/purge_haplotigs`

- Or add the Purge Haplotigs bin directory to your system PATH

```
#!text
export PATH=$PATH:/path/to/purge_haplotigs/bin
printf "\nexport PATH=\$PATH:/path/to/purge_haplotigs/bin\n" >> ~/.bashrc
```

- test

`purge_haplotigs test`

## Running Purge Haplotigs

There is a [tutorial](https://bitbucket.org/mroachawri/purge_haplotigs/wiki/Tutorial) in the wiki that you can follow which goes into a bit more detail for each step.

#### PREPARATION

Map your PacBio subreads, or some decent long reads (or even short reads) to your haploid or diploid genome assembly. 
You'll want to map a library that produces an even coverage and use a 'randombest' alignment for multimappers. 
Sort and index the bam with `samtools index`. 
Index your genome.fasta file with `samtools faidx`.
Example below uses minimap2 to map some pacbio FASTA format subreads (4 mapping threads, 1 sorting thread).

```
#!text
minimap2 -t 4 -ax map-pb genome.fa subreads.fasta.gz --secondary=no \
    | samtools sort -m 1G -o aligned.bam -T tmp.ali
```

#### STEP 1

Generate a coverage histogram by running the first script. 
This script will produce a histogram png image file for you to look at and a BEDTools 'genomecov' output file that you'll need for STEP 2.

```
#!text
purge_haplotigs  hist  -b aligned.bam  -g genome.fasta  [ -t threads ]

REQUIRED:
-b / -bam       BAM file of aligned and sorted reads/subreads to the reference
-g / -genome    Reference FASTA for the BAM file.

OPTIONAL:
-t / -threads   Number of worker threads to use, DEFAULT = 4, MINIMUM = 2
```

#### MANUAL STEP

You should have a bimodal histogram--one peak for haploid level of coverage, one peak for diploid level of coverage. 
NOTE: If you're using the phased assembly the diploid peak may be very small. 
Choose cutoffs for low coverage, low point between the two peaks, and high coverage. 
Example histograms for choosing cutoffs:

[PacBio subreads on a Diploid-phased assembly (Primary + Haplotigs)](https://bitbucket.org/repo/Ej8Mz7/images/84978409-phased_coverage_histogram.png)

[Illumina PE reads on a Haploid assembly (Primary contigs)](https://bitbucket.org/repo/Ej8Mz7/images/1039246939-coverage_histogram.png)


#### STEP 2

Run the second script using the cutoffs from the previous step to analyse the coverage on a contig by contig basis. 
This script produces a contig coverage stats csv file with suspect contigs flagged for further analysis or removal.


```
#!text
purge_haplotigs  cov  -i aligned.bam.genecov  -l <integer>  -m <integer>  -h <integer>  \
            [-o coverage_stats.csv -j 80  -s 80 ]

REQUIRED:
-i / -in        The bedtools genomecov output that was produced from 'purge_haplotigs readhist'
-l / -low       The read depth low cutoff (use the histogram to eyeball these cutoffs)
-h / -high      The read depth high cutoff
-m / -mid       The low point between the haploid and diploid peaks

OPTIONAL:
-o / -out       Choose an output file name (CSV format, DEFAULT = coverage_stats.csv)
-j / -junk      Auto-assign contig as "j" (junk) if this percentage or greater of the contig is 
                low/high coverage (DEFAULT = 80, > 100 = don't junk anything)
-s / -suspect   Auto-assign contig as "s" (suspected haplotig) if this percentage or less of the
                contig is diploid level of coverage (DEFAULT = 80)
```

#### STEP 3

Run the purging pipeline. 
This script will automatically run a BEDTools windowed coverage analysis (if generating dotplots), and minimap2 alignments to assess which contigs to reassign and which to keep. 
The pipeline will make several iterations of purging. 
Optionally, parse repeats `-r` in BED format for improved handling of repetitive regions


```
#!text
purge_haplotigs  purge  -g genome.fasta  -c coverage_stats.csv

REQUIRED:
-g / -genome        Genome assembly in fasta format. Needs to be indexed with samtools faidx.
-c / -coverage      Contig by contig coverage stats csv file from the previous step.

OPTIONAL:
-t / -threads       Number of worker threads to use. DEFAULT = 4
-o / -outprefix     Prefix for the curated assembly. DEFAULT = "curated"
-r / -repeats       BED-format file of repeats to ignore during analysis.
-d / -dotplots      Generate dotplots for manual inspection.
-b / -bam           Samtools-indexed bam file of aligned and sorted reads/subreads to the
                    reference, required for generating dotplots.

ADVANCED:
-a / -align_cov     Percent cutoff for identifying a contig as a haplotig. DEFAULT = 70
-m / -max_match     Percent cutoff for identifying repetitive contigs. Ignored when 
                    using repeat annotations (-repeats). DEFAULT = 250
-I                  Minimap2 indexing, drop minimisers every N bases, DEFAULT = 4G
-v / -verbose       Print EVERYTHING.
-limit_io           Limit for I/O intensive jobs. DEFAULT = -threads
-wind_min           Min window size for BED coverage plots (for dotplots). DEFAULT = 5000
-wind_nmax          Max windows per contig for BED coverage plots (for dotplots). DEFAULT = 200
```

#### ALL DONE!

You will have five files

- **<prefix>.fasta**: These are the curated primary contigs
- **<prefix>.haplotigs.fasta**: These are all the haplotigs identified in the initial input assembly.
- **<prefix>.artefacts.fasta**: These are the very low/high coverage contigs (identified in STEP 2). NOTE: you'll probably have mitochondrial/chloroplast/etc. contigs in here with the assembly junk. 
- **<prefix>.reassignments.tsv**: These are all the reassignments that were made, as well as the suspect contigs that weren't reassigned.
- **<prefix>.contig_associations.log**: This shows the contig "associations" e.g 

```
#!text
000000F,PRIMARY -> 000000F_005,HAPLOTIG
                -> 000000F_009,HAPLOTIG
                -> 000000F_010,HAPLOTIG
```

If generating dotplots you will get two directories:

- **dotplots_unassigned_contigs:** These are the dotplots for the suspect contigs that remain unassigned.
- **dotplots_reassigned_contigs:** These are the dotplots for the contigs that were reassigned. 


### Optional Extras

#### Trim overlapping contig ends

You may find that the ends of some contigs overlap. 
Purge Haplotigs can identify and trim these overlapping ends.
Note that this script is experimental and is still under development.
The script will find alignments between the ends of primary contigs and will 
trim the sequence from the shorter contig [(see example here)](https://bitbucket.org/repo/Ej8Mz7/images/2217546761-purgeHaplotigsClip.png).


```
#!text
purge_haplotigs  clip  -p contigs.fasta  -h haplotigs.fasta

REQUIRED:
-p / -primary       Primary contigs (curated.fasta by default) from the 'purge' stage.
-h / -haplotigs     Haplotigs (curated.haplotigs.fasta by default) from the 'purge' stage.

OPTIONAL:
-o / -outPrefix     Output file prefix. DEFAULT = clip
-l / -length        Minimum overlap length to trigger clipping. DEFAULT = 10000
-g / -gap           Maximum end gap to allow clipping. DEFAULT = 10000
-r / -ratio         Max allowable gap to alignment ratio. DEFAULT = 0.5
-t / -threads       Threds to use. DEFAULT = 4
```

#### Contig placement file and contig renaming

You may wish to submit your assembly as an NCBI haploid + alts assembly which requires a [placement file](https://www.ncbi.nlm.nih.gov/assembly/docs/submission/).
You can use the Purge Haplotigs 'place' script to automate this.
You can also use this script to rename your contigs using the FALCON Unzip naming convention.

```
#!text
purge_haplotigs  place  -p primary_contigs.fasta  -h haplotigs.fasta  \
            [ -o out.tsv  -t <INT  -c <INT  -f ]

REQUIRED:
-p / -primary       Primary contigs fasta file
-h / -haplotigs     Haplotigs fasta file

OPTIONAL:
-o / -out           Placement file name. DEFAULT = ncbi_placements.tsv
-t / -threads       Threads for Minimap2. DEFAULT = 4
-c / -coverage      Coverage cutoff percentage for pairing contigs. 
                    DEFAULT = 50 (%)
-f / -falconNaming  Rename contigs in the style used by FALCON Unzip. Saved
                    to <in-prefix>.FALC.fasta
```

#### Examine dotplots

If you generated dotplots you can go through them and check the assignments. 
You might wish to move reassigned contigs back into the primary contig pool or purge contigs that were too ambiguous for automatic reassignment. 
Below are some examples of what might occur. 

[A haplotig](https://bitbucket.org/repo/Ej8Mz7/images/2663721414-haplotig.png) - Hopefully most of your reassigned haplotigs will look like this.

[Contig is mostly haplotig](https://bitbucket.org/repo/Ej8Mz7/images/3628840007-haploid_diploid_hemizygous.png) - This example has part of the contig with a diploid level of coverage and part at haploid level with poor alignment to either reference (possibly hemizygous region).

[Haplotig with many tandem repeats](https://bitbucket.org/repo/Ej8Mz7/images/854324485-repeat_rich.png)

[Haplotig is palindrome](https://bitbucket.org/repo/Ej8Mz7/images/1079545124-haplotig_with_palindrome.png)

[Contig circling through string graph 'knots'](https://bitbucket.org/repo/Ej8Mz7/images/228451888-repeats_string_graph_short_cut.png) - while this may be a valid string graph path it will likely still confound short read mapping to the haploid assembly.

## Citation

The pipeline is published at BMC Bioinformatics:

[Purge Haplotigs: allelic contig reassignment for third-gen diploid genome assemblies](https://doi.org/10.1186/s12859-018-2485-7)


## Updates

Read the latest updates [HERE](https://bitbucket.org/mroachawri/purge_haplotigs/wiki/Updates)


